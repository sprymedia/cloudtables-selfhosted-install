#!/bin/bash

source ./utils.bash

#############################################
check_containers_stopped() {
  all_stopped="yes"
  services="cloudtables cloudtables_redis cloudtables_postgres cloudtables_nginx"
  for service in $services ; do
    progress "Checking to see if $service has been stopped"
    status=$(docker-compose ps --services --filter "status=running"  | grep "^${service}$")
    if [ -n "${status}" ] ; then
      all_stopped="no"
    fi
  done
}


#################################################################
# Entry point of script
#################################################################

source ${ENV_FILE}

delete_containers="no"
clear_config="no"
force=""

while getopts "cdfv" opt; do
  case $opt in
    c)
      clear_config="yes"
      ;;
    d)
      delete_containers="yes"
      ;;
    f)
      force="-f"
      ;;
    v)
      progress "CloudTables configurator version ${CLOUDTABLES_VERSION}"
      exit 0
      ;;
    \?) 
      echo "Usage: $0 [-cmu]
      -c: clear the configuration files
      -d: delete the containers after stopping them
      -f: force deletion (no confirmation asked)
      -v: display version of script"
      exit 1
      ;;
  esac
done

progress "CloudTables configurator version ${CLOUDTABLES_VERSION}"
progress "Stopping the Docker containers."

docker-compose stop
check_error $? "docker-compose failed to stop the containers!"

if [ "${delete_containers}" = "yes" ] ; then
  progress "Deleting the Docker containers."
  docker-compose rm ${force}
  check_error $? "docker-compose failed to delete the containers!"
fi

if [ "${clear_config}" = "yes" ] ; then
  progress "Deleting the CloudTables configuration."
  progress "Confirm all services have stopped."

  check_containers_stopped
  if [ "${all_stopped}" != "yes" ] ; then
    error "Containers are still running. Please stop them before deleting the configuration."
  fi

  rm -f .env
  check_error $? "Failed to delete the configuration!"
fi
