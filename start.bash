#!/bin/bash

cd $(dirname $0)

source utils.bash

ENV_FILE=".env"
DEFAULT_ENV_FILE="docker.env.default"
WORKING_ENV_FILE="/tmp/cloudtables.env"

DOCKER_FILE="docker-compose.yml"
DEFAULT_DOCKER_FILE="docker-compose.yml.default"
WORKING_DOCKER_FILE="/tmp/cloudtables-docker.yml"

#############################################
set_value() {
  sed -i "s/^${1}=\(.*\)$/${1}=\"${2}\"/" ${WORKING_ENV_FILE}
}

#############################################
get_random_string() {
  # Args: length default_to_use_if_present(optional)
  if [ -z "${2}" ] ; then
    tr -dc A-Za-z0-9 </dev/urandom | head -c ${1} ; echo ''
  else
    echo "${2}"
  fi
}

#############################################
configure_value() {
  # Args: Question default variable takeDefault(optional)
  if [ "${4}" = "no" -o -z "${4}" ] ; then
    ask_question "${1}" "${2}"
  else
    answer="${2}"
  fi

  set_value "${3}" "${answer}"
}

#############################################
copy_existing_env() {
  cp ${DEFAULT_ENV_FILE} ${WORKING_ENV_FILE}
}

#############################################
copy_existing_docker() {
  cp ${DEFAULT_DOCKER_FILE} ${WORKING_DOCKER_FILE}
}

#############################################
copy_default_env() {
  cp ${DEFAULT_ENV_FILE} ${WORKING_ENV_FILE}
}

#############################################
copy_default_docker() {
  cp ${DEFAULT_DOCKER_FILE} ${WORKING_DOCKER_FILE}
}

#############################################
create_working_config() {
  # Args: config (all|docker|env)
  case ${1} in
    docker) 
      copy_existing_env
      copy_default_docker
      ;;
    env) 
      copy_default_env
      copy_existing_docker
      ;;
    all) 
      copy_default_env
      copy_default_docker
      ;;
  esac
      
  modify_config="yes"
}

#############################################
release_working_config() {
  progress "Deploying configuration files (${ENV_FILE} and ${DOCKER_FILE})"

  if [ -n "${CT_DEV}" ] ; then
    set_value "CLOUDTABLES_VERSION" "latest"
  fi 

  cp ${WORKING_ENV_FILE} ${ENV_FILE}
  cp ${WORKING_DOCKER_FILE} ${DOCKER_FILE}
}

#############################################
configure_postgres() {
  ask_yesno_question "Use a Docker container for the Postgres database?" "yes"
  if [ "${answer}" = "yes" ] ; then 
    docker="true"

    set_value "${POSTGRES_HOST}" "cloudtables_postgres"

    sed -i "s/#POSTGRES_BASE//" ${WORKING_DOCKER_FILE}

    ask_yesno_question "Persist Postgres data in a docker volume?" "no"
    set_value "POSTGRES_VOLUME" "${answer}"
    if [ "${answer}" = "yes" ] ; then
      sed -i "s/#POSTGRES_VOLUME//" ${WORKING_DOCKER_FILE}
      volume="true"
    fi

    configure_value "PostgreSQL port?" "${POSTGRES_EXPOSED_PORT}" "POSTGRES_EXPOSED_PORT"
  else
    configure_value "IP address or hostname of PostgreSQL server?" "${POSTGRES_HOST}" "POSTGRES_HOST"
    configure_value "PostgreSQL port?" "${POSTGRES_PORT}" "POSTGRES_PORT"
  fi

  if [ "${docker}" != "true" -o "${volume}" = "true" ] ; then 
    ask_yesno_question "Keep any CloudTables data found in the database?" "yes"
    set_value "POSTGRES_KEEP_EXISTING" "${answer}"
  fi

  configure_value "PostgreSQL database?" "${POSTGRES_DB}" "POSTGRES_DB"
  configure_value "PostgreSQL user?" "${POSTGRES_USER}" "POSTGRES_USER"
  configure_value "PostgreSQL password?" "${POSTGRES_PASSWORD}" "POSTGRES_PASSWORD"
  POSTGRES_PASSWORD=${answer}
}

#############################################
configure_redis() {
  ask_yesno_question "Take default values for redis server?" "yes"
  redis=${answer}
  configure_value "Redis password?" "$(get_random_string 12 ${REDIS_PASS})" "REDIS_PASS" ${redis}
  configure_value "Redis session secret?" "$(get_random_string 12 ${SESS_SECRET})" "SESS_SECRET" ${redis}
}

#############################################
configure_smtp() {
  ask_yesno_question "Configure SMTP server?" "no"
  if [ "${answer}" = "yes" ] ; then
    configure_value "SMTP address?" "${CORE_SMTP_ADDR}" "CORE_SMTP_ADDR"
    configure_value "SMTP user?" "${CORE_SMTP_USER}" "CORE_SMTP_USER"
    configure_value "SMTP password?" "${CORE_SMTP_PASS}" "CORE_SMTP_PASS"
  fi
}

#############################################
configure_host() {
  # Get IP address of the host
  if [ -z "${HOST}" ] ; then
    HOST=$(hostname -I | awk '{print $1;}')
  fi

  ask_question "IP address or hostname of this host server?" "${HOST}"
  set_value "HOST" "${answer}"

  ask_question "Port to be used for the web-server?" "${PORT}"
  set_value "PORT" "${answer}"
}

#############################################
configure() {
  configure_host
  configure_postgres
  configure_redis
  configure_smtp
}

#############################################
check_version() {
  THIS_VERSION="${CLOUDTABLES_VERSION}"

  source ${ENV_FILE}

  OLD_VERSION="${CLOUDTABLES_VERSION}"

  if [ "${OLD_VERSION}" != "${THIS_VERSION}" ] ; then
    progress "Detected a change in version - current configuration is for ${OLD_VERSION} (this is ${THIS_VERSION})"
    progress "Configuration questions will be asked again in case config has changed"

    POSTGRES_KEEP_EXISTING="yes"

    create_working_config all

    UPDATE="yes"
    modify_config="yes"
  fi
}


#############################################
get_string() {
	NUM=$1
	CHAR=$2

	i=0;
	string=""

	while [ ${i} -lt ${NUM} ] ; do
		string="${string}${CHAR}"
		i=$((i+1))
	done

	echo "${string}"
}

#############################################
get_progress() {
	NOW=$1
	MAX=$2

	p="["
	p="${p}$(get_string ${NOW} '.')"
	p="${p}$(get_string $((${MAX} - ${NOW})) '#')"
	p="${p}]"

	echo $p
}

#############################################
wait_for_server_up() {
	TIMEOUT=30
	SERVER="http://${HOST}${CT_PORT}"
	progress "Waiting for server (${SERVER}) to complete initialisation"

	i=0;
	response=""

	while [ ${i} -lt ${TIMEOUT} ] && [ "${response}" != "200" ] ; do
		i=$((i+1))
		response=$(curl -L --connect-timeout 2 --max-time 2 --write-out '%{http_code}' --silent --output /dev/null ${SERVER})
		progress=$(get_progress ${i} ${TIMEOUT})
		echo -n -e "${progress}\r"
		sleep 1
	done

	echo ""

	if [ ${i} -lt ${TIMEOUT} ] ; then
		progress "Server successfully started"
	else
		error "Server failed to start - returned ${response}"
	fi
}

#################################################################
# Entry point of script
#################################################################

source ${DEFAULT_ENV_FILE}

modify_config="no"
version_check="yes"

while getopts "mnuv" opt; do
  case $opt in
    m)
      modify_config="yes"
      ;;
    n)
      version_check="no"
      ;;
    u)
      git pull
      exit $?
      ;;
    v)
      progress "CloudTables configurator version ${CLOUDTABLES_VERSION}"
      exit 0
      ;;
    \?) 
      echo "CloudTables configurator version '${CLOUDTABLES_VERSION}'"
      echo "Usage: $0 [-mu]
      -m: modify existing configuration
      -n: no version check
      -u: update these scripts
      -v: display version of script"
      exit 1
      ;;
  esac
done

progress "CloudTables configurator version ${CLOUDTABLES_VERSION}"

# See if any previous config
if [ ! -f ${ENV_FILE} ] ; then
  progress "Let's run through a few quick questions to get you started."
  create_working_config all
else
  # Check and see if running new version
  if [ -z "${CT_DEV}" -a "${version_check}" = "yes" ] ; then
    check_version
  fi

  if [ "${UPDATE}" != "yes" -a ${modify_config} = "yes" ] ; then
    ask_yesno_question "Are you sure you want to modify the existing config?" "yes"
    if [ "${answer}" = "yes" ] ; then
      progress "It is recommended you delete the current containers for the modifications to take effect."
      source ${ENV_FILE}
      create_working_config docker
    else
      modify_config="no"
    fi
  fi
fi

if [ ${modify_config} = "yes" ] ; then
  configure
  release_working_config
fi

source ${ENV_FILE}

progress "Starting the docker containers."

docker-compose up -d
check_error $? "docker-compose failed to start!"

if [ ${PORT} -ne 80 ] ; then
  CT_PORT=":${PORT}"
else
  CT_PORT=""
fi

wait_for_server_up

progress "CloudTables is up and running!"
progress "Please go to http://${HOST}${CT_PORT} in your web-browser to start using CloudTables"
progress "To stop the services, either use 'docker-compose stop', or 'stop.bash'"
progress "To restart the services, either use 'docker-compose up', or 'start.bash'"

