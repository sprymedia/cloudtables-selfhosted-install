#!/bin/bash

# Constants
ENV_FILE=".env"
DEFAULT_ENV_FILE="docker.env.default"
WORKING_ENV_FILE="/tmp/cloudtables.env"

DOCKER_FILE="docker-compose.yml"
DEFAULT_DOCKER_FILE="docker-compose.yml.default"
WORKING_DOCKER_FILE="/tmp/cloudtables-docker.yml"

#############################################
error() {
	echo "!!! FATAL ERROR: $1"
	exit 1
}

#############################################
progress() {
	echo "... $1"
}

#############################################
check_error() {
    if [ "$1" -ne 0 ] ; then
        error "$2 ($1)"
    fi
}

#############################################
_get_string() {
    NUM=$1
    CHAR=$2

    i=0;
    string=""

    while [ ${i} -lt ${NUM} ] ; do
        string="${string}${CHAR}"
        i=$((i+1))
    done

    echo "${string}"
}

#############################################
get_progress_bar() {
    NOW=$1
    MAX=$2

    p="["
    p="${p}$(_get_string ${NOW} '.')"
    p="${p}$(_get_string $((${MAX} - ${NOW})) '#')"
    p="${p}]"

    echo $p
}


#############################################
ask_question() {
  question=${1}
  default=${2}
  opts=("${!3}")

  # hiddenopts can be used so that "yes" and "no" can be accepted, but not shown
  hiddenopts=("${!4}")

  line=""

  if [ -n "${opts}" ] ; then
    line="${line} [$(IFS="|" ; echo "${opts[*]}")]"
  elif [ -n "${default}" ] ; then
    line="${line} [${default}] "
  fi

  while [ true ] ; do
    echo -n "??? ${question}${line} "
    read reply

    if [ -z "${reply}" ] ; then
      reply=${default}
    fi

    if [ -z "${opts}" ] ; then
      break;
    fi

    if [[ " ${opts[@]} " =~ " ${reply} " ]] ; then
      break;
    fi

    if [ -n "${hiddenopts}" ] ; then
      if [[ " ${hiddenopts[@]} " =~ " ${reply} " ]] ; then
        break;
      fi
    fi

  done

  answer=$reply
}

#############################################
ask_yesno_question() {
  # Args: Question default
  if [ "${2}" = "yes" ] ; then
    options=("Y" "n")
  else
    options=("y" "N")
  fi

  hiddenopts=("yes" "y" "no" "n")

  ask_question "${1}" "${2}" "options[@]" "hiddenopts[@]"

  case ${answer} in
    y|Y|yes|YES)
      answer="yes" ;;
    n|N|no|NO)
      answer="no" ;;
  esac
}
